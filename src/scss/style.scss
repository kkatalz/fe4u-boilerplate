/* Import fonts */
@import url("../fonts/Roboto.css");

/* Reset and normalize */
@import url("./normalize.css");

$color-primary: #f6755f;
$color-secondary: #f45c4c;
$color-black: black;
$color-white: white;
$color-gray: gray;

h1,
h2,
h3,
h4 {
  margin: 0;
}

body {
  padding: 30px 60px;
  font-family: "Roboto", serif;
}

.title {
  font-size: 40px;
  font-weight: bold;
  text-align: left;
  color: $color-white;
  text-shadow: -1px 0px 0px $color-primary, 2px 0px 0px $color-black,
    0px -2px 0px $color-primary, 0px 1px 0px $color-black;
  transform: scale(1.1, 1);
  transition: all 0.15s;
  cursor: pointer;
  font-family: "Alegreya", serif;
}

.title {
  &:hover {
    text-shadow: -2px 0px 0px $color-primary, 2px 0px 0px $color-black,
      0px -2px 0px $color-primary, 0px 2px 0px $color-black;
  }
}

.top-header,
.bottom-header {
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 40px;
}

.bottom-header {
  margin-bottom: 20px;
}

.search-info {
  display: flex;
  gap: 10px;
  height: 100%;
}

.search-input {
  padding: 10px 6px;
  /* padding-right: 45px; */
  width: 200px;
  transition: all 0.2s;
}

.search-input {
  &:hover {
    width: 280px;
  }
}

.search-button {
  padding: 0 14px;
  background-color: $color-white;
  border: 1px solid $color-secondary;
  color: $color-secondary;
  transition: all 0.2s;
}

.search-button {
  &:hover {
    background-color: $color-secondary;
    color: $color-white;
  }
}

.sections {
  color: $color-secondary;
  font-size: 22px;
  font-weight: 500;
  display: flex;
  gap: 20px;
}

.add-teacher-button {
  font-size: 20px;
  font-weight: 500;
  padding: 8px 16px;
}

/* MAIN BODY */

.horizontal-line {
  width: 60%;
  border-color: $color-black;
  border-width: 1px;
  margin: 30px;
}

/* TOP TEACHERS */

.teachers-container {
  display: flex;
  flex-direction: column;
  align-items: center;
  font-weight: 500;
  margin-bottom: 50px;
}

.top-teachers-label {
  margin-bottom: 20px;
}

.filters {
  align-items: center;
  display: flex;
  gap: 20px;
  margin-bottom: 50px;
}

.filters {
  div {
    padding-right: 25px;
    border-right: 1px dotted $color-black;
  }
}

.filters {
  div {
    &:last-child {
      padding-right: 0;
      border: none;
    }
  }
}

.filter {
  label {
    margin-right: 10px;
    color: $color-gray;
  }
}

.filter {
  select {
    padding: 5px;
    border-radius: 1px;
    box-shadow: 2px 2px 2px 1px rgba(0, 0, 0, 0.2);
  }
}

.filter-label {
  color: $color-gray;
}

/* teachers with photos+ name+ specialization+ country */

.teacher-info {
  display: flex;
  flex-direction: column;
  align-items: center;
}

.teachers-photo {
  object-fit: cover;
  border-radius: 125px;
  border: 2px solid $color-secondary;
  margin-bottom: 5px;
  display: flex;
  justify-content: center;
  align-items: center;
  color: $color-secondary;
  font-size: 26px;
  transition: all 0.15s;
  width: 100%;
  height: 100%;
}

.img-container {
  width: 200px;
  height: 200px;
  transition: all 0.3s;
  overflow: hidden;
  border-radius: 125px;
  border: 3px solid $color-secondary;
}

.img-add-container {
  position: relative;
}

.teachers-star {
  position: absolute;
  top: 3px;
  right: 15px;
  width: 40px;
  height: 40px;
}

.img-container {
  &:hover {
    .teachers-photo {
      transform: scale(1.2);
      border: 3px solid #df7666;
    }
  }
}

.teachers-name {
  margin-top: 10px;
  font-size: 22px;
  text-align: center;
  width: min-intrinsic;
  width: -webkit-min-content;
  width: -moz-min-content;
  width: min-content;
  display: table-caption;
  display: -ms-grid;
  -ms-grid-columns: min-content;
}

.teachers-specialization {
  margin-top: 10px;
  color: $color-gray;
  font-size: 17px;
}

.teachers-country {
  margin-top: 8px;
  font-size: 15px;
}

.teachers {
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(200px, 1fr));
  gap: 50px;
  max-width: 1200px;
  margin-inline: auto;
  position: relative;
}

/* Statisctics */
#customers {
  font-family: Arial, Helvetica, sans-serif;
  /* border-collapse: collapse; */
  width: 100%;
}

#customers td,
#customers th {
  padding: 8px;
}

.statistics-header {
  border-bottom: 2px solid $color-gray;
}

.statistics-header {
  th {
    &:hover {
      border-bottom: 3px solid $color-black;
    }
  }
}

.statistics-header {
  th {
    position: relative;
    transition: all 0.2s;
  }
}

.statistics-header {
  th {
    &:hover {
      &::after {
        content: "↓";
        width: 10px;
        height: 8px;
        position: absolute;
      }
    }
  }
}

#customers {
  tr {
    &:nth-child(odd) {
      background-color: #f7f5f8;
    }
  }
}

#customers {
  tr {
    &:hover {
      background-color: #ddd;
    }
  }
}

#customers {
  th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: center;
    background-color: $color-white;
    color: $color-black;
  }
}

/* pagination */
.pagination {
  display: flex;
  width: 100%;
  margin-top: 30px;
  gap: 15px;
  color: #27a6bd;
}

.active-page-pagination {
  color: $color-black;
}

.active-page-last {
  cursor: pointer;
}

.active-page-last {
  &:hover {
    color: $color-gray;
  }
}

/* Favorites */
.chevron {
  color: $color-gray;
  transition: 0.15s;
  cursor: pointer;
}

.chevron {
  &:hover {
    color: $color-black;
  }
}

.chevron-left {
  position: absolute;
  top: 45%;
  left: -5%;
  font-size: 30px;
  font-weight: bold;
  transform: scale(1, 4);
}

.chevron-right {
  position: absolute;
  top: 45%;
  right: -5%;
  font-size: 30px;
  font-weight: bold;
  color: $color-gray;
  transform: scale(1, 4);
}

/* FOOTER */

.footer-hr {
  display: flex;
  justify-content: center;
  margin-bottom: 15px;
}

.footer {
  display: flex;
  /* align-items: center; */
  text-align: left;
  flex-direction: column;
}

.bottom-footer {
  margin-top: 25px;
}

/* MODAL */
.modal-container {
  position: fixed;
  width: 100vw;
  height: 100vh;
  left: 0;
  top: 0;
  background: rgba(0, 0, 0, 0.8);
  z-index: 2;
}

.modal {
  width: 450px;
  background: $color-white;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  position: absolute;
  z-index: 5;
  border-radius: 10px;
}

.modal-header {
  background-color: #464648;
  display: flex;
  justify-content: space-between;
  padding: 6px;
  border-top-left-radius: 10px;
  border-top-right-radius: 10px;
}

.modal-header-add-teacher {
  color: $color-white;
}

.modal-close-window {
  color: $color-white;
  transform: rotate(45deg);
}

.modal-form {
  margin-top: 15px;
  width: 100%;
  display: flex;
  flex-direction: column;
}

.modal-form input,
.modal-form select {
  padding: 7px;
}

.modal-main {
  padding-inline: 25px;
  padding-bottom: 25px;
}

.modal-form-label {
  color: $color-gray;
  padding-top: 8px;
  text-align: left;
  margin-bottom: 5px;
}

.modal-extra-info {
  display: grid;
  width: 100%;
  grid-template-columns: minmax(20px, 1fr) minmax(20px, 1fr);
  column-gap: 15px;
}

.gender {
  width: 50%;
  display: flex;
  flex-direction: row;
  align-items: center;
}

.sex {
  margin-right: 20px;
}

/* Male or Female ? */

.modal-checkbox {
  appearance: none;
  /* Removes default appearance */
  -webkit-appearance: none;
  /* For older versions of Safari */
  -moz-appearance: none;
  /* For Firefox */
  width: 5px;
  height: 5px;
  border: 2px solid #aaa;
  border-radius: 50%;
  background-color: #fff;
  vertical-align: middle;
  /* Optional: to align it with adjacent content */
  cursor: pointer;
  margin-right: 7px;
}

.modal-checkbox {
  &:checked {
    background-color: dodgerblue;
    border-color: dodgerblue;
  }
}

.extraSpace {
  margin-right: 30px;
}

.modal-checkbox {
  border-radius: 50%;
  width: 10px;
  height: 10px;
}

.modal-button {
  width: 100%;
  padding: 10px;
  margin-top: 30px;
  font-size: 18px;
  font-weight: bold;
}

/* SECOND MODAL TEACHER INFO */

.modal-teacher-info {
  padding: 10px;
}

.modal-teacher-info-top {
  width: 100%;
  display: flex;
  gap: 20px;
  margin-bottom: 30px;
  text-align: left;
}

.modal-teacher-photo {
  width: 220px;
  height: 220px;
  object-fit: cover;
}

/* DETAILS ON THE RIGHT SIDE */

.modals-top-details {
  text-align: left;
}

.modal-teacherName {
  margin-bottom: 20px;
  white-space: nowrap;
}

.modal-specialization {
  font-weight: bold;
  margin-bottom: 10px;
}

.modal-reachWithTeacher {
  margin-bottom: 10px;
}

.email {
  color: #41adbd;
}

.modal-desc {
  text-align: left;
}

.modal-info {
  width: 600px;
}

.btn-toggle {
  font-size: 15px;
  border: none;
  background: transparent;
  color: #91bad0;
  border-bottom: 1px dotted #41adbd;
  margin-right: auto;
  padding-inline: 0;
  margin-top: 10px;
}

.modal-button-toggle {
  display: flex;
  margin-top: 0;
}

.modal-name {
  display: flex;
  width: 100%;
  justify-content: space-between;
  /* align-items: center; */
}

.modal-name-img {
  width: 40px;
  height: 30px;
  margin-top: 5px;
  margin-left: 120px;
}

.footer-text {
  float: left;
}

@media screen and (max-width: 768px) {
  .top-header,
  .bottom-header {
    flex-direction: column;
    gap: 20px;
  }

  .teachers {
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
  }

  .filters {
    flex-wrap: wrap;
    justify-content: center;
  }
}
